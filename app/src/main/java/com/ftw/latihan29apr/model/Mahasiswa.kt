package com.ftw.latihan29apr.model

data class Mahasiswa (val nama: String, val nim: Int, val jurusan: String)