package com.ftw.latihan29apr.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ftw.latihan29apr.R
import com.ftw.latihan29apr.model.Mahasiswa
import kotlinx.android.synthetic.main.item_view.view.*

class AdapterRv(private val data: MutableList<Mahasiswa> = mutableListOf()) :
    RecyclerView.Adapter<AdapterRv.ViewHolder>() {

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(mahasiswa: Mahasiswa, position: Int) {
            view.tv_nama.text = mahasiswa.nama
            view.tv_jurusan.text = mahasiswa.jurusan
            view.tv_nim.text = mahasiswa.nim.toString()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_view, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       holder.bind(data[position],position)
    }

    fun setData(list: List<Mahasiswa> = mutableListOf<Mahasiswa>()) {
        data.clear()
        data.addAll(list)
        notifyDataSetChanged()
    }
}