package com.ftw.latihan29apr.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager

import com.ftw.latihan29apr.R
import com.ftw.latihan29apr.adapter.AdapterRv
import com.ftw.latihan29apr.model.Mahasiswa
import com.ftw.latihan29apr.presenter.InterfaceContract
import com.ftw.latihan29apr.presenter.PresenterMahasiswa
import kotlinx.android.synthetic.main.fragment_home.*

/**
 * A simple [Fragment] subclass.
 */
class HomeFragment : Fragment(), InterfaceContract.View {
    private var adapterMahasiswa: AdapterRv = AdapterRv(mutableListOf<Mahasiswa>())

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rv_mahasiswa.layoutManager =
            LinearLayoutManager(view.context, LinearLayoutManager.VERTICAL, false)
        rv_mahasiswa.adapter = adapterMahasiswa
        val presenterMahasiswa = PresenterMahasiswa(this)

        btnSubmit.setOnClickListener {
            presenterMahasiswa.insertData()
        }
    }

    override fun showData(mahasiswa: List<Mahasiswa>) {
        adapterMahasiswa.setData(mahasiswa)
    }

}
