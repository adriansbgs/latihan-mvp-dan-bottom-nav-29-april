package com.ftw.latihan29apr.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.ftw.latihan29apr.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        navigateFragment(HomeFragment())
        supportActionBar?.title = "Home"

        bottom_nav.itemIconTintList = null
        bottom_nav.setOnNavigationItemSelectedListener {
            when (it.itemId) {

                R.id.home_menu -> {
                    navigateFragment(HomeFragment())
                    supportActionBar?.title = "Home"
                }
                R.id.favorite_menu -> {
                    navigateFragment(FavoriteFragment())
                    supportActionBar?.title = "Favorite"
                }
                R.id.profile_menu -> {
                    navigateFragment(ProfileFragment())
                    supportActionBar?.title = "Profile"
                }
            }
            true
        }
    }

    private fun navigateFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.container, fragment)
            commit()
        }
    }
}
