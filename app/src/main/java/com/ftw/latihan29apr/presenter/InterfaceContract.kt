package com.ftw.latihan29apr.presenter

import com.ftw.latihan29apr.model.Mahasiswa

interface InterfaceContract {
    interface View {
        fun showData(mahasiswa: List<Mahasiswa>)
    }

    interface Presenter {
        fun insertData()
    }
}