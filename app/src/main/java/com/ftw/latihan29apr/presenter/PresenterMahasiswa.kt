package com.ftw.latihan29apr.presenter

import com.ftw.latihan29apr.model.Mahasiswa

class PresenterMahasiswa (private val view: InterfaceContract.View) : InterfaceContract.Presenter {
    override fun insertData() {
        val data = mutableListOf<Mahasiswa>().apply {
            add(Mahasiswa("Juned",113,"Informatika"))
            add(Mahasiswa("Suep",124,"Komunikasi"))
            add(Mahasiswa("Supratman",254,"Fisika"))
        }
        view.showData(data)
    }
}